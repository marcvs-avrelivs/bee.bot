#!/usr/bin/env node
'use strict';
const keys = require('./keys.js'); // login credentials*/
const twitter = require('twitter');
const fs = require('fs');
const path = __dirname+"/bee.movie";
const c = require('random-coordinates');



function postTweet(status) {
  const client = new twitter(keys);
  return new Promise((resolve, reject) => {
      const [lat, long] = c({fixed: 8}).split(/, /);
      const opts = {status, lat, long};
      client.post('statuses/update', opts, function(error, tweet, response) {
          if(!error) {
              resolve(true);
            } else {
        
              console.dir(error);
              reject(error);
            }
          })
      })
}
function getScript() {
  return fs.readFileSync(path).toString().split(/\n/);

}

function writeScript(script) {
  fs.writeFileSync(path, script.join("\n"));
}

(async () => {
  const script = getScript();
  const line = script.shift();
  const tweet = await postTweet(line);
  if(tweet === true) {
    console.log(line);
    if(script[script.length-1] == '') { script.pop(); }
    script.push(line);
    writeScript(script);
  }
})();
